#!/bin/bash
#
# Prépararation fichiers diff pour livraison en evironnement CVS/Eclipse
#
# Actions :
#
# - Suppression mention fichiers binaires (non supporté par diffs Eclipse)
# - Suppression de 'a/' et 'b/' en entête des paths (principe non supporté par Eclipse).
# - Suppression des fichiers spécifiques Mercurial (.hg*), qui ne doivent pas être intégrés sous CVS.
#
# Usage:
#
# 	livdiff <nom fichier diff>
#
# Sortie:
# 
# Dans le même répertoire, produit:
#
# * Un fichier de sortie dont le nom est relatif au fichier d'entrée:
# - Si avec extension: <non fichier>-cvs.<extension>
# - Si sans extension: <nom fichier>-cvs

# * Si le diff contient des binaires, un fichier ReadMe au format Mardown qui signale les binaires à intégrer manuellement. Il est nommé:
#
# 	<nom fichier diff>.ReadMe.md
# 
 
set -e 

 
error() {
  local -i code="${1:-1}"
  >&2 echo "Error: ${2:-code ${code}}"
  exit "${code}"
}

 
diff="$1"
[ -d "${diff}" ] && diff="${diff}.diff"
[ -f "${diff}" ] || error 1 "File '${diff}' not found"


REGEX='^(([^_-]+)(-[^.]+)?_to_([^.-]+)(-[^.]+)?).diff'
[[ ! "$( basename ${diff} )" =~ $REGEX ]] && error 1 'Invalid diff file name. Tags not found ( "<FROM>_to_<TO>.diff" pattern )'
patch="${BASH_REMATCH[1]}"
oldTag="${BASH_REMATCH[2]}"
oldVer="${BASH_REMATCH[3]}"
newTag="${BASH_REMATCH[4]}"
newVer="${BASH_REMATCH[5]}"


dir="$( dirname ${diff} )"
config="${dir}/livdiff.conf"

[ -f "${config}" ] || error 1 "Configuration not found for ${project}: ${config}"
. "${config}" || error 2 "Unable to load '${config}'"

[ -z "${src}" ] &&  error 2 "Source project is not defined in '${config}'. Add 'src=<path to source project>'"
[ -d "${src}" ] || error 2 "Project source folder not found ( '${src}' ). Check '${config}'."
[ -z "${project}" ] &&  error 2 "Project is not defined in '${config}'. Add 'project=<project name>'."


curTag="$(hg identify -t -R ${src} )"
ON_TAG='(^| )'"${newTag}"'($| )'
[[ -z "${curTag}" || ! "${curTag}" =~ $ON_TAG ]] && error 2 "The current version shoud be on tag '${newTag}' ( at '${src}' )." 

branch="$(hg identify -b -R ${src})"
[[ "${branch}" == "default" ]] && branchName='le tronc' || branchName="la branche ${branch}"

>/dev/null hg log -r ${oldTag} -R "${src}" || error 2 "The tag '${oldTag}' did not exists ( in '${src}' )."

patchDir="${dir}/${patch}"
cvs="${patchDir}/patch-cvs.diff"
readMe="${patchDir}/readMe.md"
bin="${patchDir}/${project}"

[ -d "${patchDir}" ] && { rm -r "${patchDir}" || error 2 "Unable to delete ${patchDir}"; }
mkdir "${patchDir}"


DIFF_REGEX='^diff\ -r\ [a-z0-9]+\ -r\ [a-z0-9]+\ '
HG_FILE_REGEX='\.hg[a-z0-9]*$'
AB_REGEX='^(---|\+\+\+) [ab]/(.+)$'
BINARY_REGEX='^Binary file (.+) has changed$'

buffered=false
buffer=""

flush() {
 [ "$buffered" = true ] && echo "$buffer" >> "$cvs"
 buffered=false
}

discardBuffer() {
  buffered=false
}

out() {
  flush
  buffer="$1"
  buffered=true
}

# is this line a diff command ?
diffStarts() {
 [[ "$1" =~  $DIFF_REGEX ]]
}

needsFilter() {
  [[ "$1" =~ $HG_FILE_REGEX ]]
}

addBin() {
   mkdir -p  "$(dirname ${bin}/$1)" && cp "${src}/$1" "${bin}/$1"
   return 0
}

removeBin() {
  [ -z "$removeBin" ] && echo "
* __Supprimer les fichiers suivants__ (suppression système __et__ CVS):
" >> "$readMe";
  removeBin=true
  echo "     * \`${1}\`"  >> "$readMe"
}

binaryNotification() {
  [[ ! "$line" =~ $BINARY_REGEX ]] && return 1
  local file="${BASH_REMATCH[1]}"
  discardBuffer
  [ -f "${src}/${file}" ] &&  addBin "${file}" || removeBin "${file}"
  return 0
}

removeAB() {
  if [[ "$1" =~ $AB_REGEX ]]
  then
    out "${BASH_REMATCH[1]} ${BASH_REMATCH[2]}"
  else
    out "$1"
  fi
}

>"${readMe}" echo "

# Patch ${oldTag}${oldVer} -> ${newTag}${newVer}

Le répertoire contient les fichiers nécessaires à l'application du patch.
  
## Mode opératoire

* Sous CVS, se positionner sur ${branchName}.

* Faire un update sur le tag \`${oldTag}\`

     * Ça doit impérativement être le head.
"

[[ "${oldTag}" == "${newTag}" ]] && >>"${readMe}" echo "

* __Supprimer le tag "${oldTag}__".

"

filtered=false
IFS=''
while read -r line
do
  # handle diff files filtering
  if diffStarts "$line"
  then
    if needsFilter "$line"
    then
      filtered=true
      continue
    else
      filtered=false
    fi
  elif [ "$filtered" = true ]
  then
    continue
  fi
  binaryNotification "$line" && continue
  removeAB "$line"
done < "$diff"

echo "
* Se positionner sur le répertoire racine de votre projet ${project}.
" >> "$readMe";

if [ -d "${bin}" ]
then
    echo "
* Y copier le contenu de ce répertoire \`./${project}\`

     * Il contient les nouveaux binaires (ajoutés ou modifiés).
     * Ces versions écrasent les fichiers existants.
" >> "$readMe";

fi

echo "
* Appliquer le patch ( fichier \`$(basename $cvs)\` ).

* Commiter, __sans rien oublier__ (fichiers supprimés, modifiés ou ajoutés).

* Poser le tag \`${newTag}\`.

* Tester une refab de cadre avec cette nouvelle version pour s’assurer que tout est ok.

## En cas d'incident

Ne pas tenter de bricoler, mais m’appeler. Le plus important, c’est que nos référentiels respectifs restent toujours synchronisés, à la virgule près.
" >> "$readMe";
